package com.druzzers.b16.services.impl;

import com.druzzers.b16.entities.User;
import com.druzzers.b16.repositories.mongo.UserRepository;
import com.druzzers.b16.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserSerivceImpl implements UserService {

    @Autowired
    private UserRepository userRepository;

    public User findById(String id) {
        return userRepository.findById(id);
    }

    public User newUser(User user) {
        return userRepository.save(user);
    }
}
