package com.druzzers.b16.services.impl;

import com.druzzers.b16.entities.News;
import com.druzzers.b16.repositories.mongo.NewsRepository;
import com.druzzers.b16.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Comparator;
import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {

    @Autowired
    private NewsRepository newsRepository;

    @Override
    public News findById(String id) {
        return newsRepository.findOne(id);
    }

    @Override
    public News add(News news) {
        return newsRepository.save(news);
    }

    @Override
    public News edit(News news) {
        return null;
    }

    @Override
    public boolean delete(String id) {
        newsRepository.delete(id);
        return newsRepository.findOne(id) == null;
    }

    @Override
    public List<News> getAllNews() {
        return newsRepository.findAll();
    }

    @Override
    public List<News> sortNewsByDate() {
        return null;
    }

    @Override
    public List<News> sortNewsByLikes() {
        return null;
    }

    static Comparator<News> dateIncrease = new Comparator<News>() {

        public int compare(News newsOne, News newsTwo) {
            return newsOne.getDateOfPublication().compareTo(newsTwo.getDateOfPublication());
        }
    };
}