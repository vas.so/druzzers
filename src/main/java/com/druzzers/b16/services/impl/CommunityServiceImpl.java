package com.druzzers.b16.services.impl;

import com.druzzers.b16.entities.Community;
import com.druzzers.b16.repositories.mongo.CommunityRepository;
import com.druzzers.b16.services.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CommunityServiceImpl implements CommunityService {

    @Autowired
    CommunityRepository communityRepository;

    @Override
    public Community newCommunity(Community community) {
        return communityRepository.save(community);
    }

    @Override
    public Community findCommunityById(String id) {
        return communityRepository.findOneById(id);
    }

    @Override
    public Community findCommunityByName(String name) {
        return communityRepository.findOneByName(name);
    }

    @Override
    public Community update(Community community) {
        return communityRepository.save(community);
    }

    @Override
    public void delete(String id) {
        communityRepository.delete(id);
    }
}