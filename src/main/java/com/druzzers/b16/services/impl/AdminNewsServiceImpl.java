package com.druzzers.b16.services.impl;

import com.druzzers.b16.entities.News;
import com.druzzers.b16.repositories.mongo.NewsRepository;
import com.druzzers.b16.services.AdminNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AdminNewsServiceImpl implements AdminNewsService {

    @Autowired
    NewsRepository newsRepository;

    @Override
    public List<News> badNews() {
        return newsRepository.findAll();
    }
}