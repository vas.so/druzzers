package com.druzzers.b16.services;

import com.druzzers.b16.entities.User;

public interface UserService {

    User findById(String id);
    User newUser(User user);
}
