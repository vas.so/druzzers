package com.druzzers.b16.services;

import com.druzzers.b16.entities.News;

import java.util.List;

public interface NewsService {

    News findById(String id);
    News add(News news);
    News edit(News news);
    boolean delete(String id);

    List<News> getAllNews();
    List<News> sortNewsByDate();
    List<News> sortNewsByLikes();
}