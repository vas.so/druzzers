package com.druzzers.b16.services;

import com.druzzers.b16.entities.Community;
import org.springframework.data.mongodb.core.query.Query;

import java.util.List;

public interface CommunityService {

    Community newCommunity(Community community);

    Community findCommunityById(String id);
    Community findCommunityByName(String name);

    void delete(String id);
    Community update(Community community);
}