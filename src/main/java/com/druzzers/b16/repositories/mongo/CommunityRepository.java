package com.druzzers.b16.repositories.mongo;

import com.druzzers.b16.entities.Community;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.List;

public interface CommunityRepository extends MongoRepository<Community, String> {

    Community findOneById(String id);
    Community findOneByName(String name);
    Community save(Community community);
    void delete(String id);
}
