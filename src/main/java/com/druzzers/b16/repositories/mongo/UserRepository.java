package com.druzzers.b16.repositories.mongo;

import com.druzzers.b16.entities.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {

    User findById(String id);
    User save(User user);
}
