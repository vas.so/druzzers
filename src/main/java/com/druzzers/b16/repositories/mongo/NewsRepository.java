package com.druzzers.b16.repositories.mongo;

import com.druzzers.b16.entities.News;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Date;
import java.util.List;

public interface NewsRepository extends MongoRepository<News, String> {

    News findById(String id);
    News save(News news);
}