package com.druzzers.b16.controllers;

import com.druzzers.b16.entities.News;
import com.druzzers.b16.services.NewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @PostMapping("/add")
    public News addNews(@RequestBody News news) {
        return newsService.add(news);
    }

    @PostMapping("/delete/{id}")
    public boolean deleteNews(@PathVariable String id) {
        return newsService.delete(id);
    }

    @PostMapping("/edit")
    public News editNews(@RequestBody News news) {
        return newsService.edit(news);
    }

    @GetMapping("/{id}")
    public News findById(@PathVariable String id) {
        return newsService.findById(id);
    }
}