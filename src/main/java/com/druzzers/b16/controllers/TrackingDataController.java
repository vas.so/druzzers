package com.druzzers.b16.controllers;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/track")
public class TrackingDataController {

    @GetMapping("/myip")
    public String clientIp(HttpServletRequest request) {

        String ip = request.getHeader("X-FORWARDED-FOR");

        if (ip == null)
            ip = request.getRemoteAddr();

        return ip;
    }
}
