package com.druzzers.b16.controllers;

import com.druzzers.b16.entities.User;
import com.druzzers.b16.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping("/{id}")
    public User getUser(@PathVariable String id) {
        return userService.findById(id);
    }

    @RequestMapping(value = "/new",
                    method = RequestMethod.POST,
                    consumes = "application/json")
    public User newUser(@RequestBody User user) {
        return userService.newUser(user);
    }
}