package com.druzzers.b16.controllers;

import com.druzzers.b16.entities.Community;
import com.druzzers.b16.services.CommunityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/community")
public class CommunityController {

    @Autowired
    CommunityService communityService;

    @GetMapping("/find")
    public Community findCommunityById(@RequestParam(name = "id") String id,
                                       @RequestParam(name = "name") String name) {
        if (!id.isEmpty()) {
            return communityService.findCommunityByName(name);
        } else if (!name.isEmpty()) {
            return communityService.findCommunityById(id);
        } else {
            return null;
        }
    }

    @PostMapping("/new")
    public Community newCommunity(@RequestBody Community community) {
        return communityService.newCommunity(community);
    }

    @PostMapping("/update")
    public Community updateCommunity(@RequestBody Community community) {
        return communityService.update(community);
    }

    @PostMapping("/delete")
    public String deleteCommunity(@RequestParam(name = "id") String id) {
        communityService.delete(id);
        return communityService.findCommunityById(id) == null ? "deleted": "can`t delete community with given id";
    }
}