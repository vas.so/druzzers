package com.druzzers.b16.controllers;

import com.druzzers.b16.security.sso.google.GoogleSSOAuth2Provider;
import com.druzzers.b16.security.sso.intita.IntITASSOAuth2Provider;
import com.druzzers.b16.security.sso.intita.userData.IntITAUser;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

@RestController
@RequestMapping("/login")
public class LoginController {

    final static Logger logger = Logger.getLogger("LoginController");

    @RequestMapping("/facebook")
    public String facebookLogin() {
        return null;
    }

    @RequestMapping("/google")
    public String googleplusLogin(@RequestParam(name="code") String code, HttpServletRequest httpServletRequest) throws IOException {

//        GoogleSSOAuth2Provider googleSSOAuth2Provider = new GoogleSSOAuth2Provider(true, code);
//        return googleSSOAuth2Provider.doStuff(httpServletRequest);
        GoogleSSOAuth2Provider googleSSO = new GoogleSSOAuth2Provider(code);
        String accessToken = googleSSO.getAccessToken();
        return googleSSO.getUserCredentials(accessToken);
    }

    @RequestMapping("/intita")
    public IntITAUser intitaLogin(@RequestParam(name="code") String code, HttpServletRequest httpServletRequest) throws IOException {

        IntITASSOAuth2Provider intITASSOAuth2Provider = new IntITASSOAuth2Provider(true, code);
        return intITASSOAuth2Provider.doStuff(httpServletRequest);
    }
}