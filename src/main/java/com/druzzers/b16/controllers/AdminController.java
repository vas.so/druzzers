package com.druzzers.b16.controllers;

import com.druzzers.b16.entities.News;
import com.druzzers.b16.services.AdminNewsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/admin")
public class AdminController {

    @Autowired
    AdminNewsService adminNewsService;

    @GetMapping("/news")
    public List<News> badNews() {
        return adminNewsService.badNews();
    }
}