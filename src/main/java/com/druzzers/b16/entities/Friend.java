package com.druzzers.b16.entities;

import lombok.Data;

@Data
public class Friend {

    private User friend;

    public Friend(User user) {
        this.friend = user;
    }
}