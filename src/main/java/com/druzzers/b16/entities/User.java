package com.druzzers.b16.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Set;

@Data
@Document
public class User {

    @Id
    private String id;

    private String email;
    private String password;

    private PersonalData personalData;
    private TrackingData trackingData;
    private Set<News> newsList;
    private Set<User> friends;

    public User(User user) {

        this.email = user.email;
        this.password = user.password;
        this.personalData = user.personalData;
        this.trackingData = user.trackingData;
        this.newsList = user.newsList;
        this.friends = user.friends;
    }

    protected User() {};
}