package com.druzzers.b16.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.Set;

@Data
@Document
public class TrackingData {

    @Id
    private String id;

    private Set<Login> loginData;
    private boolean isOnline;

    private class Login {

        @Id
        private String id;
        private String ip;
        private Date loginTime;
        private String country;
        private String browser;
    }
}