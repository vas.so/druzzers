package com.druzzers.b16.entities;

import com.druzzers.b16.utils.DateWrapper;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class Comment {

    @Id
    private String id;

    private String userId;
    private String dateOfPublication;
    private String text;
    private List<String> images;

    public Comment(Comment comment) {
        this.userId = comment.userId;
        this.dateOfPublication = DateWrapper.getCurrentDate();
        this.text = comment.text;
        this.images = comment.images;
    }

    protected Comment() {}
}