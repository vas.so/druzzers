package com.druzzers.b16.entities;

import com.druzzers.b16.utils.DateWrapper;
import lombok.Data;
import org.springframework.data.annotation.Id;
import java.util.List;

@Data
public class News {

    @Id
    private String id;

    private String userId;
    private String topic;
    private String dateOfPublication;
    private boolean isAnonymous;
    private String text;
    private Integer likes;
    private List<String> tags;
    private List<String> images;
    private List<Comment> comments;
    private List<String> complains;

    public News(News news) {
        this.userId = news.userId;
        this.topic = news.topic;
        this.dateOfPublication = DateWrapper.getCurrentDate();
        this.isAnonymous = news.isAnonymous;
        this.text = news.text;
        this.tags = news.tags;
        this.images = news.images;
        this.comments = news.comments;
        this.complains = news.complains;
    }

    protected News() {};
}