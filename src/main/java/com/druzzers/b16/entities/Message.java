package com.druzzers.b16.entities;

import lombok.Data;

@Data
public class Message {

    private String fromUserId;
    private String toUserId;
    private String dateOfSending;
    private String text;
    private String image;

    public Message(Message message) {
        this.dateOfSending = message.dateOfSending;
        this.fromUserId = message.fromUserId;
        this.image = message.image;
        this.text = message.text;
        this.toUserId = message.toUserId;
    }

    protected Message() {}
}
