package com.druzzers.b16.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Data
@Document
public class Community {

    @Id
    private String id;
    private String name;
    private String purpose;
    private String logo;
    private String headerImage;

    private List<User> members;
    private List<User> contacts;
    private List<News> news;
    private List<String> discussion;
    private List<String> images;

    public Community(Community community) {
        this.id = community.id;
        this.name = community.name;
        this.purpose = community.purpose;
        this.logo = community.logo;
        this.headerImage = community.headerImage;
        this.members = community.members;
        this.contacts = community.contacts;
        this.news = community.news;
        this.discussion = community.discussion;
        this.images = community.images;
    }

    protected Community() {}
}
