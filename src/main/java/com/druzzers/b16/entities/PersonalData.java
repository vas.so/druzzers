package com.druzzers.b16.entities;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Document
public class PersonalData{

    @Id
    private String id;

    private String name;
    private String surname;
    private String gender;
    private String dateOfBirth;
    private String country;
    private String city;
    private String phone;

    public PersonalData(PersonalData personalData) {

        this.name = personalData.name;
        this.surname = personalData.surname;
        this.gender = personalData.gender;
        this.dateOfBirth = personalData.dateOfBirth;
        this.country = personalData.country;
        this.city = personalData.city;
        this.phone = personalData.phone;
    }

    protected PersonalData() {};
}