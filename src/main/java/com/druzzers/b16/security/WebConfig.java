package com.druzzers.b16.security;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
public class WebConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .antMatcher("/login")
                .authorizeRequests()
                .antMatchers("/", "/login/**")
                .permitAll()
                .anyRequest()
                .authenticated();
    }
}
