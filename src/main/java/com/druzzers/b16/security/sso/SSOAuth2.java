package com.druzzers.b16.security.sso;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public interface SSOAuth2 {

    String getAccessToken() throws IOException;
    String getUserCredentials(String accessToken);
}