package com.druzzers.b16.security.sso.intita;

import com.druzzers.b16.security.sso.SSOAuth2;
import com.druzzers.b16.security.sso.intita.userData.IntITAUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

public class IntITASSOAuth2Provider implements SSOAuth2 {

    private static final String authTokenUri = "https://sso.intita.com/oauth/token";
    private static final String userInfoUri = "https://sso.intita.com/api/user/detailed";
    private static final String grant_type = "authorization_code";
    private static final String client_id = "18";
    private static final String client_secret = "7hoMk5FyllNpVOPB6IgvxZiAQKEPqGsI12VElKvA";
    private static final String redirect_uri = "https://ea295447.ngrok.io/login/intita";

    private static Logger logger = null;
    private String code = null;

    public IntITASSOAuth2Provider(String redirectCode) {
        this.code = redirectCode;
    }

    public IntITASSOAuth2Provider(boolean logToConsole, String redirectCode) {
        logger = logToConsole ? Logger.getLogger(this.getClass().getName()) : null;
        this.code = redirectCode;
    }

    @Override
    public String getAccessToken() throws IOException {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", this.code);
        map.add("grant_type", grant_type);
        map.add("client_id", client_id);
        map.add("client_secret", client_secret);
        map.add("redirect_uri", redirect_uri);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(authTokenUri, HttpMethod.POST, httpEntity, String.class);

        ObjectMapper mapper = new ObjectMapper();
        IntITABearer googleBearer = mapper.readValue(responseEntity.getBody(), IntITABearer.class);

        return googleBearer.getAccess_token();
    }

    @Override
    public String getUserCredentials(String accessToken) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json");
        httpHeaders.add("Authorization", String.format("Bearer %s", accessToken));

        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(userInfoUri, HttpMethod.GET, httpEntity, String.class);

        return responseEntity.getBody();
    }

    public IntITAUser getIntITAUser(String userCredentials) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(userCredentials, IntITAUser.class);
    }

    private IntITABearer getIntITABearer(String authCode) throws IOException {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", authCode);
        map.add("grant_type", grant_type);
        map.add("client_id", client_id);
        map.add("client_secret", client_secret);
        map.add("redirect_uri", redirect_uri);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(authTokenUri, HttpMethod.POST, httpEntity, String.class);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(responseEntity.getBody(), IntITABearer.class).setData(responseEntity.getBody());
    }

    public IntITAUser doStuff(HttpServletRequest httpServletRequest) throws IOException {

        if (logger != null) {
            logger.info("User has just grant access to his data");
            logger.info("The path is ");
            logger.info(redirect_uri + httpServletRequest.getQueryString());

            IntITABearer intITABearer = getIntITABearer(this.code);

            logger.info("IntITA Auth JSON: ");
            logger.info(intITABearer.toString());

            String userCredentials = getUserCredentials(intITABearer.getAccess_token());
            logger.info("IntITA user entity: ");
            logger.info(userCredentials);

            return getIntITAUser(userCredentials);
        } else {

            String accessToken = this.code;
            String userCredentials = getUserCredentials(getAccessToken());
            return getIntITAUser(userCredentials);
        }
    }
}