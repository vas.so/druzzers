package com.druzzers.b16.security.sso.google;

import com.druzzers.b16.security.sso.SSOAuth2;
import com.druzzers.b16.security.sso.google.userData.GoogleUser;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;

@Data
public class GoogleSSOAuth2Provider implements SSOAuth2 {

    private static final String authTokenUri = "https://accounts.google.com/o/oauth2/token";
    private static final String userInfoUri = "https://www.googleapis.com/oauth2/v1/userinfo";
    private static final String grantType = "authorization_code";
    private static final String clientId = "481988276035-dchq7heop3rcnlhtm2m81qcojfnp5uki.apps.googleusercontent.com";
    private static final String clientSecret = "JsArz0D_IdeAkrSX6m6llfpe";
    private static final String redirectUri = "https://ea295447.ngrok.io/login/google";

    private String code = null;
    private static Logger logger = null;

    public GoogleSSOAuth2Provider(String redirectCode) {
        this.code = redirectCode;
    }

    public GoogleSSOAuth2Provider(boolean logToConsole, String redirectCode) {
        logger = logToConsole ? Logger.getLogger(this.getClass().getName()) : null;
        this.code = redirectCode;
    }

    @Override
    public String getAccessToken() throws IOException {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", this.code);
        map.add("grantType", grantType);
        map.add("clientId", clientId);
        map.add("clientSecret", clientSecret);
        map.add("redirectUri", redirectUri);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(authTokenUri, HttpMethod.POST, httpEntity, String.class);

        ObjectMapper mapper = new ObjectMapper();
        GoogleBearer googleBearer = mapper.readValue(responseEntity.getBody(), GoogleBearer.class);

        return googleBearer.getAccess_token();
    }

    @Override
    public String getUserCredentials(String accessToken) {

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.add("Content-Type", "application/json");
        httpHeaders.add("Authorization", String.format("Bearer %s", accessToken));

        HttpEntity<String> httpEntity = new HttpEntity<>(httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(userInfoUri, HttpMethod.GET, httpEntity, String.class);

        return responseEntity.getBody();
    }

    public GoogleUser getGoogleUser(String userCredentials) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(userCredentials, GoogleUser.class);
    }

    private GoogleBearer getGoogleBearer(String authCode) throws IOException {

        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("code", authCode);
        map.add("grantType", grantType);
        map.add("clientId", clientId);
        map.add("clientSecret", clientSecret);
        map.add("redirectUri", redirectUri);

        HttpHeaders httpHeaders = new HttpHeaders();
        httpHeaders.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        HttpEntity<MultiValueMap<String, String>> httpEntity = new HttpEntity<>(map, httpHeaders);
        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> responseEntity = restTemplate.exchange(authTokenUri, HttpMethod.POST, httpEntity, String.class);

        ObjectMapper mapper = new ObjectMapper();

        return mapper.readValue(responseEntity.getBody(), GoogleBearer.class).setData(responseEntity.getBody());
    }

    // https://accounts.google.com/o/oauth2/auth?scope=https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email&state=profile&response_type=code&client_id=481988276035-dchq7heop3rcnlhtm2m81qcojfnp5uki.apps.googleusercontent.com&redirect_uri=https://ea295447.ngrok.io/login/google
    public GoogleUser doStuff(HttpServletRequest httpServletRequest) throws IOException {

        if (logger != null) {
            logger.info("User has just grant access to his data");
            logger.info("The path is ");
            logger.info(redirectUri + httpServletRequest.getQueryString());

            logger.info(this.code);
            GoogleBearer googleBearer = getGoogleBearer(this.code);

            logger.info("Google Auth JSON");
            logger.info(googleBearer.toString());

            String userCredentials = getUserCredentials(googleBearer.getAccess_token());
            logger.info("Google user entity");
            logger.info(userCredentials);

            return getGoogleUser(userCredentials);
        } else {

            String accessToken = this.code;
            String userCredentials = getUserCredentials(getAccessToken());
            return getGoogleUser(userCredentials);
        }
    }
}