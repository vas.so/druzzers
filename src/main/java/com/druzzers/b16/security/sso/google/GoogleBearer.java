package com.druzzers.b16.security.sso.google;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class GoogleBearer {

    @JsonIgnore
    private String data;

    private String access_token;
    private String expires_in;
    private String id_token;
    private String token_type;

    public GoogleBearer() {}

    public GoogleBearer(String JSON) {
        this.data = JSON;
    }

    public GoogleBearer setData(String json) {
        this.data = json;
        return this;
    }

    public String toString() {
        return data;
    }
}
