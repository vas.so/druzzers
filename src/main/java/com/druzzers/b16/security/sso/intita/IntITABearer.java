package com.druzzers.b16.security.sso.intita;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

@Data
public class IntITABearer {

    @JsonIgnore
    private String data;

    private Long userUID;
    private String token_type;
    private String expires_in;
    private String access_token;
    private String refresh_token;

    public IntITABearer setData(String json) {
        this.data = json;
        return this;
    }

    public String toString() {
        return
                this.userUID +
                this.expires_in +
                this.token_type +
                this.access_token +
                this.refresh_token;
    }
}
