package com.druzzers.b16.security.sso.intita.userData;

import lombok.Data;

@Data
public class Trainer {

    private String firstName;
    private String middleName;
    private String secondName;
    private String email;
}
