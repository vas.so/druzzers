package com.druzzers.b16.security.sso.intita.userData;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

import java.util.List;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class IntITAUser {

    private String firstName;
    private String middleName;
    private String secondName;
    private String email;
    private String facebook;
    private String googleplus;
    private String linkedin;
    private String vkontakte;
    private String twitter;
    private String phone;
    private String address;
    private String education;
    private String interests;
    private String aboutUs;
    private String aboutMy;
    private String avatar;
    private String skype;
    private String country;
    private String city;
    private String educationForm;
    private List<Trainer> trainers;
}