package com.druzzers.b16.security.sso.builder;

import java.lang.reflect.Field;

public class SSOConnectionsBuilder<T> {

    private String authTokenUri = "https://sso.intita.com/oauth/token";
    private String userInfoUri = "https://sso.intita.com/api/user/detailed";

    private String grant_type = "authorization_code";
    private String client_id = "18";
    private String client_secret = "7hoMk5FyllNpVOPB6IgvxZiAQKEPqGsI12VElKvA";
    private String redirect_uri = "https://ea295447.ngrok.io/login/intita";

    public SSOConnectionsBuilder() {}

    public SSOConnectionsBuilder setAuthTokenUri(String authTokenUri) {
        this.authTokenUri = authTokenUri;
        return this;
    }

    public SSOConnectionsBuilder setUserInfoUri(String userInfoUri) {
        this.userInfoUri = userInfoUri;
        return this;
    }

    public SSOConnectionsBuilder setRedirectUri(String redirect_uri) {
        this.redirect_uri = redirect_uri;
        return this;
    }

    public SSOConnectionsBuilder setClientId(String clientId) {
        this.client_id = clientId;
        return this;
    }

    public SSOConnectionsBuilder setClientSecret(String clientSecret) {
        this.client_secret = clientSecret;
        return this;
    }

    public T build() {

        T t = null;
        try {
            t = (T)t.getClass().newInstance();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        Field []fields = t.getClass().getDeclaredFields();

        for (Field f: fields) {

            try {
                if (f.equals(this.authTokenUri)) {
                    f.set((Object)authTokenUri, (Object)authTokenUri);
                } else if (f.equals(userInfoUri)) {
                    f.set((Object)userInfoUri, (Object)userInfoUri);
                } else if (f.equals(client_id)) {
                    f.set((Object)client_id, (Object)client_id);
                } else if (f.equals(client_secret)) {
                    f.set((Object)client_secret, (Object)client_secret);
                } else if (f.equals(redirect_uri)) {
                    f.set((Object)redirect_uri, (Object)redirect_uri);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return t;
    }
}