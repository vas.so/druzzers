package com.druzzers.b16.utils;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class DateWrapper {

    private static final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss dd/MM/yyyy");

    public static LocalDateTime getCurrentLocalDateTime() {
        return LocalDateTime.now();
    }

    public static String getCurrentDate() {
        return dtf.format(getCurrentLocalDateTime());
    }
}
